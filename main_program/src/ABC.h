#ifndef LAB_1_3_ABC_H
#define LAB_1_3_ABC_H

#include <vector>
#include <string>
#include <utility>
#include <fstream>
#include <algorithm>
#include "WordInfo.h"

namespace firstLab {
    class ABC {
    public:
        void input(std::ifstream& inputFile) {
            std::string word;
            int startWordNumber = 1;
            WordInfo wordInfo;

            while (!inputFile.eof()) {
                inputFile >> word;
                std::transform(word.begin(), word.end(), word.begin(), ::toupper);
                wordInfo = std::make_pair(word, startWordNumber);
                abc.push_back(wordInfo);
            }
        }
        void calcAndRemoveSameWords() {
            auto abcRunnerIter = abc.begin();
            auto abcGeneralIter = abc.begin();
            auto abcEndIter = abc.end();
            std::string currentWord;
            int currentWordNumber;

            std::sort(abcGeneralIter, abcEndIter);

            //Подсчитать количество повторений каждого слова (это точно не O(n^2), а O(n))
            while (abcRunnerIter != abcEndIter) {
                currentWord = abcRunnerIter->first;
                currentWordNumber = 1;
                abcRunnerIter++;
                while (abcRunnerIter != abcEndIter && abcRunnerIter->first == currentWord) {
                    currentWordNumber++;
                    abcRunnerIter++;
                }
                abcGeneralIter->first = currentWord;
                abcGeneralIter->second = currentWordNumber;
                abcGeneralIter++;
            }

            //Удаляем слова после последнего уникаьлного слова
            abc.erase(abcGeneralIter, abcEndIter);
        }
        void prepareToPrint() {
            std::sort(abc.begin(), abc.end(), [](const WordInfo& leftVal, const WordInfo& rightVal) {
                return leftVal.second > rightVal.second; });
        }
        void printCommonWords(std::ofstream& outputFile) {
            outputFile << "Number of common word(s): ";
            if (abc.size() == 0) {
                outputFile << 0;
                return;
            }

            int maxWordNumber = abc[0].second;
            auto abcIter = abc.begin();
            auto abcEnd = abc.end();

            outputFile << maxWordNumber << '\n';
            while (abcIter != abcEnd && abcIter->second == maxWordNumber) {
                outputFile << abcIter->first << '\n';
                abcIter++;
            }
        }
    private:
        std::vector<WordInfo> abc;
    };
}
#endif
