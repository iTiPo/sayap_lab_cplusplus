#include <fstream>
#include <iostream>
#include "ABC.h"
#include <ctime>

using namespace std;

int main(int argc, char* argv[]) {
    try {
        if (argc != 3)
            throw int(argc);

        firstLab::ABC abc;
        ifstream inputFile(argv[1]);
        ofstream outputFile(argv[2]);

        unsigned int startTime = clock();
        abc.input(inputFile);
        abc.calcAndRemoveSameWords();
        abc.prepareToPrint();
        unsigned int endTime = clock();

        outputFile << "Time: " << endTime - startTime << '\n';
        abc.printCommonWords(outputFile);

        inputFile.close();
        outputFile.close();
    } catch (const int& exc) {
        cout << "Invalid arguments' number: " << exc << '\n';
    }
    return EXIT_SUCCESS;
}