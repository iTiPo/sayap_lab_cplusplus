/*#include "ABC.h"
#include <algorithm>

using namespace std;

void firstLab::ABC::input(std::ifstream &inputFile) {
    string word;
    int startWordNumber = 1;
    WordInfo wordInfo;

    while (!inputFile.eof()) {
        inputFile >> word;
        transform(word.begin(), word.end(), word.begin(), ::toupper);
        wordInfo = make_pair(word, startWordNumber);
        abc.push_back(wordInfo);
    }
}

void firstLab::ABC::calcAndRemoveSameWords() {
    auto abcRunnerIter = abc.begin();
    auto abcGeneralIter = abc.begin();
    auto abcEndIter = abc.end();
    string currentWord;
    int currentWordNumber;

    sort(abcGeneralIter, abcEndIter);

    while (abcRunnerIter != abcEndIter) {
        currentWord = abcRunnerIter->first;
        currentWordNumber = 1;
        abcRunnerIter++;
        while (abcRunnerIter != abcEndIter && abcRunnerIter->first == currentWord) {
            currentWordNumber++;
            abcRunnerIter++;
        }
        abcGeneralIter->first = currentWord;
        abcGeneralIter->second = currentWordNumber;
        abcGeneralIter++;
    }

    //Удаляем слова после последнего уникаьлного слова
    abc.erase(abcGeneralIter, abcEndIter);
}

void firstLab::ABC::prepareToPrint() {
    sort(abc.begin(), abc.end(), [](const WordInfo& leftVal, const WordInfo& rightVal) {
        return leftVal.second > rightVal.second; });
}

void firstLab::ABC::printCommonWords(std::ofstream &outputFile) {
    outputFile << "Number of common word(s): ";
    if (abc.size() == 0) {
        outputFile << 0;
        return;
    }

    int maxWordNumber = abc.front().second;
    auto abcIter = abc.begin();
    auto abcEnd = abc.end();

    outputFile << maxWordNumber << '\n';
    while (abcIter != abcEnd && abcIter->second == maxWordNumber) {
        outputFile << abcIter->first << '\n';
        abcIter++;
    }
}*/
