#ifndef LAB_1_3_WORDINFO_H
#define LAB_1_3_WORDINFO_H

#include <string>
#include <utility>

namespace firstLab {
    typedef std::pair<std::string, int> WordInfo;
}

#endif //LAB_1_3_WORDINFO_H
