#include <fstream>
#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char const *argv[])
{
	try {
		if (argc != 3)
			throw int(0);

		ofstream outputFile(argv[1]);
		int wordsNumber = atoi(argv[2]);

		for (int i = wordsNumber; i > 1; i--)
			outputFile << i << '\n';
		if (wordsNumber > 0)
			outputFile << 1;

		outputFile.close();
	} catch (const int&) {
		cout << "Invalid arguments' number\n";
	}

	return 0;
}